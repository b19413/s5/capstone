//! [I]

SELECT customerName FROM `customers` WHERE country LIKE "%Philippines";

//! [II]

SELECT contactLastName, contactFirstName FROM customers WHERE customerName LIKE "%La Rochelle Gifts%";

//! [III]

SELECT productName, MSRP FROM products WHERE productName LIKE "%The Titanic%";


//! [IV]

SELECT firstName, lastName FROM employees WHERE email LIKE "%jfirrelli@classicmodelcars.com%";


//! [V]

SELECT customerName FROM customers WHERE state IS NULL;

//! [VI]

SELECT firstName, lastName, email FROM employees 
        WHERE firstName LIKE "%Steve%" AND lastName LIKE "%Patterson%";

//! [VII]

SELECT customerName, country, creditLimit FROM customers 
        WHERE creditLimit > 3000 AND country != "USA";


//! [VIII]

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%"; 

//! [IX]

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

//! [X]

SELECT DISTINCT country FROM customers;

//! [XI]

SELECT DISTINCT status FROM orders;

//! [XII]

SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

//! [XIII]

SELECT firstName, lastName, city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE offices.officeCode = 5;

//! [XIV]

SELECT customerName FROM customers
	JOIN offices ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE employees.employeeNumber = 1166;

//! [XV]

SELECT productName, customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customers.customerNumber = 121;
//! [XVI]

SELECT DISTINCT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
	JOIN offices ON customers.country = offices.country
	JOIN employees ON offices.officeCode = employees.officeCode;

//! [XVII]

SELECT productName, quantityInStock FROM products
	WHERE productLine = "Planes" AND quantityInStock < 1000;

//! [XVIII]

SELECT customerName FROM customers WHERE phone LIKE "+81%";

